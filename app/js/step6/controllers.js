'use strict';

bookApp.controller('BookListCtrl', function($scope, Books) {
    $scope.books = Books.query();
});

bookApp.controller('BookCardCtrl', function($scope, Book) {
    $scope.book = Book.query();
})
