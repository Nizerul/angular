angular.module('booksServices', ['ngResource']).
    factory('Books', function($resource){
  return $resource('data/books.json', {}, {
    query: {method:'GET', isArray:true}
  });
});

angular.module('bookServices', ['ngResource']).
    factory('Book', function($resource){
  return $resource('data/book.json', {}, {
    query: {method:'GET', isArray:false}
  });
});